@extends('layouts.auth')

@section('content')
    <div class="row justify-content-center">
        <div id="channel" class="col-md-12"></div>
        @if(Auth::user()->role != 'user')
            <input id="message" class="col-md-11" type="text" placeholder="Your Message ...">
        @endif
    </div>
@endsection