@extends('layouts.profile')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card mt-4">
                <div class="card-header">
                    <h3>My Profile</h3>
                </div>
                <div class="card-body text-center">
                    <img class="rounded mb-4" src="{{url(Auth::user()->photo_path)}}" width="128px" height="128px">
                    <form method="post" action="{{route('update')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Profile Image</label>
                            <input type="file" name="photo">
                        </div>
                        <div class="form-group">
                            <input class="form-control btn submit" type="submit" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection