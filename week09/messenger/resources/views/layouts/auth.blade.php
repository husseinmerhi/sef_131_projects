<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    @auth
        <script>
            var id = "{{ Auth::user()->id }}";
            var name = "{{ Auth::user()->name }}";
            var photo = "{{ Auth::user()->photo_path }}";
            var channel = "{{ $data['channel'] }}";
        </script>
    @endauth
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/messenger.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">
</head>
<body>
<div id="app">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar purple navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <div class="white mr-4">
                        <strong>SlackOverFlow</strong>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                            @if(count($data['channels']) > 0)
                                @foreach($data['channels'] as $channel)
                                    <a href="{{route($channel->name)}}">
                                        <li class="mr-4 grey">{{$channel->name}}</li>
                                    </a>
                                @endforeach
                            @endif
                            <a href="{{route('profile')}}">
                                <li class="mr-4 grey">Profile</li>
                            </a>
                            @if(Auth::user()->role != 'user')
                                <a href="{{ route('settings') }}">
                                    <li class="mr-4 grey">Channels</li>
                                </a>
                            @endif
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            <li class="nav-item dropdown">
                                <a class="white" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <main>
                @yield('content')
            </main>
        </div>
    </div>
</div>
</body>
</html>
