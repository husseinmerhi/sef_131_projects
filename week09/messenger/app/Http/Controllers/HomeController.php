<?php

namespace messenger\Http\Controllers;

use Illuminate\Http\Request;
use messenger\Channel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $channels = Channel::all();
        $channel = Channel::where('name', 'General')->first();
        $data = [
            'channels' => $channels,
            'channel' => $channel->id
        ];
        return view('home')->with('data', $data);
    }
}