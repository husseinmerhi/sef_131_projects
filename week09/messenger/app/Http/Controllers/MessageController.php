<?php

namespace messenger\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use messenger\Message;

class MessageController extends Controller
{
    public function save(Request $request) {
        $message = new Message();
        $message->user_id = Auth::user()->id;
        $message->channel_id = $request->input('channel');
        $message->content = $request->input('content');
        $message->save();
    }

    public function load(Request $request) {
        $messages = Message::where('channel_id', $request->input('channel'))->get();
        foreach($messages as $message) {
            $data[] = array(
                'name' => $message->user->name,
                'photo_path' => $message->user->photo_path,
                'content' => $message->content,
                'channel' => $message->channel_id,
                'time' => $message->created_at
            );
        }
        $history = json_encode($data);
        echo $history;
    }
}
