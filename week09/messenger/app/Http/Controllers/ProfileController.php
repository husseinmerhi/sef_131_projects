<?php

namespace messenger\Http\Controllers;

use Illuminate\Http\Request;
use messenger\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index() {
        return view('profile');
    }

    public function update(Request $request) {
        $this->validate($request, [
            'photo' => 'required|image|mimes:jpg,jpeg,png'
        ]);

        if($request->hasFile('photo')){

            //get filename with the extension
            $filenameWithExt = $request->file('photo')->getClientOriginalName();

            //get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            //get just extension
            $extension = $request->file('photo')->getClientOriginalExtension();

            //filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            //upload image
            $path = $request->file('photo')->storeAs('public/img', $fileNameToStore);
        }

        $user = User::find(Auth::user()->id);
        $user->photo_path = '/storage/img/' . $fileNameToStore;
        $user->save();
        return redirect('/profile');
    }
}
