<?php

namespace messenger\Http\Controllers;

use Illuminate\Http\Request;
use messenger\Channel;

class AnnouncementController extends Controller
{
    public function index() {
        $channels = Channel::all();
        $channel = Channel::where('name', 'Announcement')->first();
        $data = [
            'channels' => $channels,
            'channel' => $channel->id
        ];
        return view('announcement')->with('data', $data);
    }
}


