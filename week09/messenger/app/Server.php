<?php

date_default_timezone_set('UTC');

require_once("../vendor/autoload.php");

use Devristo\Phpws\Framing\WebSocketFrame;
use Devristo\Phpws\Framing\WebSocketOpcode;
use Devristo\Phpws\Messaging\WebSocketMessageInterface;
use Devristo\Phpws\Protocol\WebSocketTransportInterface;
use Devristo\Phpws\Server\IWebSocketServerObserver;
use Devristo\Phpws\Server\UriHandler\WebSocketUriHandler;
use Devristo\Phpws\Server\WebSocketServer;

class ChatHandler extends WebSocketUriHandler {

    public function onConnect(WebSocketTransportInterface $user){

    }

    public function onMessage(WebSocketTransportInterface $user, WebSocketMessageInterface $msg) {
        foreach($this->getConnections() as $client){
            $client->sendString($msg->getData());
        }
    }
}

$loop = \React\EventLoop\Factory::create();
$logger = new \Zend\Log\Logger();
$writer = new Zend\Log\Writer\Stream("php://output");
$logger->addWriter($writer);
$server = new WebSocketServer("tcp://0.0.0.0:12345", $loop, $logger);
$router = new \Devristo\Phpws\Server\UriHandler\ClientRouter($server, $logger);
$router->addRoute('#^/slack#i', new ChatHandler($logger));
$server->bind();
$loop->run();

?>
