<?php

namespace messenger;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function user() {
        return $this->belongsTo('messenger\User');
    }
}
