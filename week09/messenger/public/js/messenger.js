window.onload = function() {
    load();
    serverHandler();
    eventHandler();
}

var socket;

function serverHandler() {
    socket = createSocket("ws://192.168.88.225:12345/slack");
    socket.onopen = function(msg) {
        setTimeout(send('Joined'),6000);
    };
    socket.onmessage = function(msg) {
        var data = JSON.parse(msg.data);
        if (data.channel == channel) {
            log(data);
        }
        adjustView();
    };
    socket.onclose = function(msg) {
        send('Disconnected');
    };
}

function createSocket(host) {
    if ('WebSocket' in window)
        return new WebSocket(host);
    else if ('MozWebSocket' in window)
        return new MozWebSocket(host);
    throw new Error("No web socket support in browser!");
}

function send(msg) {
    var time = new Date();
    var data = {
        'id': id,
        'name': name,
        'date': time.toDateString(),
        'time': time.getHours()+":"+time.getMinutes(),
        'channel': channel,
        'photo': photo,
        'message': msg
    }
    socket.send(JSON.stringify(data));
    if (data.message != 'Disconnected' && data.message != 'Joined') {
        save(data.message);
    }
}

function log(data) {
    var html = '<div class="list-group-item list-group-item-action border-bottom-0 border-top-0">' +
        '<div class="media">' +
        '<img class="mr-3" src="' + data.photo + '" width="48" height="48">' +
        '<div class="media-body">' +
        '<p class="head mt-0"><strong class="mr-1">' + data.name + '</strong><small class="time"> ' + data.time + '</small></p>' +
        '<p>' + data.message + '</p>' +
        '</div></div></div>';
    document.getElementById("channel").innerHTML += html;
}

function save(msg) {
    var data = 'content=' + msg + '&channel=' + channel;
    var xhttp = new XMLHttpRequest();
    var token = document.getElementsByTagName('meta')['csrf-token'].content;
    xhttp.open("POST", "message/save", true);
    xhttp.setRequestHeader("X-CSRF-Token",token);
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.send(data);
}

function load() {
    var data = 'channel=' + channel;
    var xhttp = new XMLHttpRequest();
    var token = document.getElementsByTagName('meta')['csrf-token'].content;
    xhttp.open("POST", "message/load", true);
    xhttp.setRequestHeader("X-CSRF-Token",token);
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.send(data);
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var messages = JSON.parse(xhttp.response);
            messages.forEach( function(item) {
                if (item.channel == channel) {
                    var html = '<div class="list-group-item list-group-item-action border-bottom-0 border-top-0">' +
                        '<div class="media">' +
                        '<img class="mr-3" src="' + item.photo_path + '" width="48" height="48">' +
                        '<div class="media-body">' +
                        '<p class="head mt-0"><strong class="mr-1">' + item.name + '</strong><small class="time"> ' + getTime(item.time.date) + '</small></p>' +
                        '<p>' + item.content + '</p>' +
                        '</div></div></div>';
                    document.getElementById("channel").innerHTML += html;
                    adjustView();
                }
            })
        }
    };
}

function eventHandler() {
    document.getElementById('message').addEventListener('keypress', function(event) {
        if (event.keyCode == 13) {
            send(event.target.value);
            event.target.value = '';
        }
    });
}

function adjustView() {
    document.getElementById('channel').scrollTop = document.getElementById('channel').scrollHeight;
}

function getDate(string) {
    return new Date(string).toDateString();
}

function getTime(string) {
    return new Date(string).getHours() + ':' + new Date(string).getMinutes();
}