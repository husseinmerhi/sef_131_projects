<?php

use Illuminate\Database\Seeder;
use messenger\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'hussein';
        $user->email = 'hussein@gmail.com';
        $user->role = 'admin';
        $user->password = 'admin@123';
        $user->save();
    }
}
