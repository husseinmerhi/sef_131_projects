<?php

use Illuminate\Database\Seeder;
use messenger\Channel;

class ChannelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $channel = new Channel();
        $channel->name = 'General';
        $channel->save();
        //
        $channel = new Channel();
        $channel->name = 'Announcement';
        $channel->save();
    }
}
