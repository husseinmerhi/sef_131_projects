<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthenticationController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('General');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/update', 'ProfileController@update')->name('update');
Route::get('/announcement', 'AnnouncementController@index')->name('Announcement');
Route::get('/settings', 'SettingsController@index')->name('settings');
Route::post('/message/save', 'MessageController@save');
Route::post('/message/load', 'MessageController@load');
