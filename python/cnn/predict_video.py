from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import cv2
import os

i = 1

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

ap = argparse.ArgumentParser()
ap.add_argument('-t', '--type', required=True, help='type should be video or cam')
ap.add_argument('-m', '--model', required=True, help='path to trained model')
ap.add_argument('-v', '--video', required=False, help='path to video')
args = vars(ap.parse_args())

type = args['type']
model = load_model(args['model'])
if type == 'video':
  cam = cv2.VideoCapture(args['video'])
  length = int(cam.get(cv2.CAP_PROP_FRAME_COUNT))
  while i <= length:
    ok, frame = cam.read()
    image = cv2.resize(frame, (32, 32))
    image = image.astype("float") / 255.0
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    (notAlarm, alarm) = model.predict(image)[0]
    label = "alarm" if alarm > notAlarm else "not alarm"
    proba = alarm if alarm > notAlarm else notAlarm
    label = "{}: {:.2f}%".format(label, proba * 100)
    output = imutils.resize(frame, width=400)
    cv2.putText(output, label, (10, 25),  cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 0), 2)
    cv2.imshow("frame", output)
    if cv2.waitKey(1) % 256 == 27:
      break
    i += 1

elif type == 'cam':
  cam = cv2.VideoCapture(0)
  while True:
    ok, frame = cam.read()
    image = cv2.resize(frame, (32, 32))
    image = image.astype("float") / 255.0
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    (notAlarm, alarm) = model.predict(image)[0]
    label = "alarm" if alarm > notAlarm else "not alarm"
    proba = alarm if alarm > notAlarm else notAlarm
    label = "{}: {:.2f}%".format(label, proba * 100)
    output = imutils.resize(frame, width=400)
    cv2.putText(output, label, (10, 25),  cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 0), 2)
    cv2.imshow("frame", output)
    if cv2.waitKey(1) % 256 == 27:
      break
    i += 1

cam.release()
cv2.destroyAllWindows()