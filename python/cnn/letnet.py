# import the necessary packages
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras import backend as K

class LeNet:
  @staticmethod
  def build(width, height, depth, classes):
    # initialize the model
    model = Sequential()
    inputShape = (height, width, depth)

    # if we are using "channels first", update the input shape
    if K.image_data_format() == "channels_first":
      inputShape = (depth, height, width)

    # first set of CONV => RELU => POOL layers
    model.add(Conv2D(filters = 6, 
                 kernel_size = 5, 
                 strides = 1, 
                 activation = 'relu', 
                 input_shape = inputShape))
    model.add(MaxPooling2D(pool_size = 2, strides = 2))

    # second set of CONV => RELU => POOL layers
    model.add(Conv2D(filters = 16, 
                 kernel_size = 5,
                 strides = 1,
                 activation = 'relu',
                 input_shape = (14,14,6)))
    model.add(MaxPooling2D(pool_size = 2, strides = 2))

    model.add(Flatten())
    # first set of FC => RELU layers
    model.add(Dense(units = 120, activation = 'relu'))
    # first set of FC => RELU layers
    model.add(Dense(units = 84, activation = 'relu'))
    # softmax classifier
    model.add(Dense(units = classes, activation = 'softmax'))

    # return the constructed network architecture
    return model