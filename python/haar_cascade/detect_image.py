import os
import sys
import cv2
import numpy
import argparse

ap = argparse.ArgumentParser()
ap.add_argument('-i', '--img', required=True, help='name of the image')
args = vars(ap.parse_args())

cascade = cv2.CascadeClassifier('data/cascade.xml')

img_path = 'test_set/single/' + args['img'] + '.jpg'
detect_path = 'detected/single/' + args['img'] + '.jpg'
img = cv2.imread(img_path)
objects = cascade.detectMultiScale(img, 1.05, 6, minSize=(30, 30))
if len(objects) == 0:
  print('object not present')
  cv2.imwrite(detect_path, img)
else:
  print('object detected')
  for(x, y, w, h) in objects:
    cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 5)
    cv2.imwrite(detect_path, img)