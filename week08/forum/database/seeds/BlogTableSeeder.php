<?php

use Illuminate\Database\Seeder;
use forum\Blog;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blog = new Blog();
        $blog->title = 'Title1';
        $blog->body = 'Body1';
        $blog->user_id = '1';
        $blog->created_at = '2018-04-18 13:20:11';
        $blog->save();
        $blog = new Blog();
        $blog->title = 'Title2';
        $blog->body = 'Body2';
        $blog->user_id = '2';
        $blog->created_at = '2018-04-18 13:21:11';
        $blog->save();
        $blog = new Blog();
        $blog->title = 'Title3';
        $blog->body = 'Body3';
        $blog->user_id = '1';
        $blog->created_at = '2018-04-18 13:22:11';
        $blog->save();
        $blog = new Blog();
        $blog->title = 'Title4';
        $blog->body = 'Body4';
        $blog->user_id = '2';
        $blog->created_at = '2018-04-18 13:23:11';
        $blog->save();
        $blog = new Blog();
        $blog->title = 'Title5';
        $blog->body = 'Body5';
        $blog->user_id = '1';
        $blog->created_at = '2018-04-18 13:24:11';
        $blog->save();
        $blog = new Blog();
        $blog->title = 'Title6';
        $blog->body = 'Body6';
        $blog->user_id = '2';
        $blog->created_at = '2018-04-18 13:25:11';
        $blog->save();
        $blog = new Blog();
        $blog->title = 'Title7';
        $blog->body = 'Body7';
        $blog->user_id = '1';
        $blog->created_at = '2018-04-18 13:26:11';
        $blog->save();
        $blog = new Blog();
        $blog->title = 'Title8';
        $blog->body = 'Body8';
        $blog->user_id = '2';
        $blog->created_at = '2018-04-18 13:27:11';
        $blog->save();
    }
}
