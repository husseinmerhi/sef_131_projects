<?php

use Illuminate\Database\Seeder;
use forum\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'hussein';
        $user->email = 'hussein@gmail.com';
        $user->password = Hash::make('admin@123');
        $user->save();
        $user = new User();
        $user->name = 'fatime';
        $user->email = 'fatime@gmail.com';
        $user->password = Hash::make('admin@123');
        $user->save();
    }
}
