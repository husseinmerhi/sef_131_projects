@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- You are logged in! -->
                    @include('inc.message')
                    @if (count($data) > 0)
                        @foreach($data as $blog)
                            <h4><a href="/post/{{$blog->id}}">{{$blog->title}}</a></h4>
                            <small>{{$blog->created_at}}</small>
                            <p>{{$blog->body}}</p>
                        @endforeach
                        {{$data->links()}}
                    @else
                        <div class="alert alert-danger">No Blogs Available</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
