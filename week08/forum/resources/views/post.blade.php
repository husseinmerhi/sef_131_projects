@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h4>{{$data->title}}</h4>
                    <small>{{$data->created_at}}</small>
                    <p>{{$data->body}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection