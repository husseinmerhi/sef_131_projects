<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/post/{id}', 'PostController@index');
Route::get('/create', 'CreateController@index');
Route::post('/create', 'CreateController@save');
Route::get('/inventory', 'InventoryController@index');
Route::get('/inventory/{id}', 'InventoryController@remove');