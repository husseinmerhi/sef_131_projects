<?php

namespace forum\Http\Controllers;

use Illuminate\Http\Request;
use forum\Blog;

class PostController extends Controller
{
    public function index($id){
        $data = Blog::find($id);
        return view('post')->with('data', $data);
    }
}
