<?php

namespace forum\Http\Controllers;

use Illuminate\Http\Request;
use forum\Blog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Blog::orderBy('created_at','desc')->paginate(3);
        return view('home')->with('data', $data);
    }
}
