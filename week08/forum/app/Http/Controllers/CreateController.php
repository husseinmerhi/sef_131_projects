<?php

namespace forum\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use forum\Blog;

class CreateController extends Controller
{
    public function index(){
        return view('create');
    }

    public function save(Request $request){
        $this->validate($request, [
            'title' => 'required|max:100',
            'body' => 'required'
        ]);

        $blog = new Blog();
        $blog->title = $request->input('title');
        $blog->body = $request->input('body');
        $blog->user_id = Auth::user()->id;
        $blog->save();
        return redirect('/inventory')->with('success', 'Blog Created!');
    }
}
