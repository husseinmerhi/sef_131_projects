<?php

namespace forum\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use forum\Blog;

class InventoryController extends Controller
{
    public function index(){
        $data = Blog::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(3);
        return view('inventory')->with('data', $data);
    }

    public function remove($id){
        $blog = Blog::find($id);
        $blog->delete();
        return redirect('inventory')->with('success', 'Blog Removed');
    }
}
