window.onload = function() {
    //like button
    var button = document.getElementsByClassName('like')[0];
    button.addEventListener('click', function(event) {
        var data = 'post=' + event.target.id;
        var xhttp = new XMLHttpRequest();
        var token = document.getElementsByTagName('meta')['csrf-token'].content;
        xhttp.open("POST", "like", true);
        xhttp.setRequestHeader("X-CSRF-Token",token);
        xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xhttp.send(data);
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                if (xhttp.responseText == 'like') {
                    var old = document.getElementsByClassName('btn')[0].innerHTML.split(' ')[1]
                    var likes = parseInt(old) + 1;
                    document.getElementsByClassName('btn')[0].innerHTML = "&hearts; " + likes;
                    console.log(this.responseText);
                }
                else {
                    var old = document.getElementsByClassName('btn')[0].innerHTML.split(' ')[1]
                    var likes = old - 1;
                    document.getElementsByClassName('btn')[0].innerHTML = "&hearts; " + likes;
                    console.log(this.responseText);
                }
            }
        };
    });
}