window.onload = function() {
    //like button
    var button = document.getElementsByClassName('follow')[0];
    update(button.id);
    button.addEventListener('click', function(event) {
        var data = 'target=' + event.target.id;
        var xhttp = new XMLHttpRequest();
        var token = document.getElementsByTagName('meta')['csrf-token'].content;
        xhttp.open("POST", "follow", true);
        xhttp.setRequestHeader("X-CSRF-Token",token);
        xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xhttp.send(data);
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                if (xhttp.responseText == 'follow') {
                    update(button.id);
                }
                else {
                    update(button.id);
                }
            }
        };
    });
}

function update(id){
    var data = 'target=' + id;
    var xhttp = new XMLHttpRequest();
    var token = document.getElementsByTagName('meta')['csrf-token'].content;
    xhttp.open("POST", "update", true);
    xhttp.setRequestHeader("X-CSRF-Token",token);
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.send(data);
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById('follower').innerText = this.responseText.split(',')[0];
            document.getElementsByClassName('follow')[0].innerText = this.responseText.split(',')[1];
        }
    };
}