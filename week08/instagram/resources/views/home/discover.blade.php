@extends('layouts.app2')

@section('content')
    <div class="row mt-4 justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    <h4>Search For Your Friends</h4>
                </div>
                <div class="card-body">
                    @if(count($users) > 0)
                        @foreach($users as $user)
                            <div class="row">
                                <div class="col">
                                    <img src="{{url('storage/img/'.$user->profile->photo)}}" width="64px" height="64px">
                                    <p class="mt-2">{{$user->name}}</p>
                                </div>
                                <div class="col">
                                    <a class="btn btn-outline-primary" href="{{route('view', ['id' => $user->id])}}">View Profile</a>
                                </div>
                            </div>
                        @endforeach
                        {{$users->links()}}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection