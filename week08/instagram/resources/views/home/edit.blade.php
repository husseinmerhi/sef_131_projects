@extends('layouts.app2')

@section('content')
    <div class="card mt-4">
        <div class="card-header">
            <div class="row mt-4 justify-content-center">
                <div class="col-md-2">
                    <img class="rounded-circle" src="{{url('storage/img/'.$user->profile->photo)}}" width="128px" height="128px">
                </div>
                <div class="col-md-4">
                    <div class="row text-center">
                        <div class="col mt-4">
                            <strong><p>{{count($user->posts)}}</p><p>Posts</p></strong>
                        </div>
                        <div class="col mt-4">
                            <strong> <p>{{count($user->followers)}}</p><p>Followers</p></strong>
                        </div>
                        <div class="col mt-4">
                            <strong><p>{{count($user->followed)}}</p><p>Following</p></strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 justify-content-center">
                <div class="col-md-6">
                    <p>
                        <img src="{{url('img/'.$user->profile->country->photo)}}" width="32" height="32">
                        <strong>{{$user->profile->username}} <small>({{$user->profile->gender}})</small></strong><br>
                        <small>Telephone: {{$user->profile->telephone}}</small>
                    </p>
                    <p>{{$user->profile->bio}}</p>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-md-5">
                    @include('layouts.message')
                    <form method="POST" action="{{route('update')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <lablel>Username</lablel>
                            <input class="form-control" type="text" name="username">
                        </div>
                        <div class="form-group">
                            <lablel>Telephone</lablel>
                            <input class="form-control" type="text" name="telephone">
                        </div>
                        <div class="form-group">
                            <lablel>Gender</lablel>
                            <select class="form-control" name="gender">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Country</label>
                            <select id="countries" class="form-control" name="country">
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Profile Image</label>
                            <input type="file" name="photo">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="bio" placeholder="Describe Yourself ..." rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <input class="form-control btn btn-primary" type="submit" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/countries.js') }}"></script>
@endsection