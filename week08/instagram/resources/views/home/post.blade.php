@extends('layouts.app2')

@section('content')

    <div class="row mt-4 justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4>Share Your Photo</h4>
                </div>
                <div class="card-body">
                    @include('layouts.message')
                    <form method="POST" action="{{route('submit')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Photo</label>
                            <input type="file" name="photo">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="caption" rows="5" placeholder="Caption ..."></textarea>
                        </div>
                        <input class="btn btn-primary btn-block" type="submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection