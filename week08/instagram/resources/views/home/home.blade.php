@extends('layouts.app2')

@section('content')
    <div class="row justify-content-center" >
        <div class="col-md-6">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center mb-4">
        <div class="col-md-6">
            @if(count($user->followed) > 0)
                @foreach($user->followed as $follower)
                    @if(count($follower->userFollowed->posts) > 0)
                        @foreach($follower->userFollowed->posts as $post)
                            <div class="card mt-4">
                                <div class="card-header">
                                    <img class="rounded-circle mr-4" src="{{url('storage/img/'.$post->user->profile->photo)}}" width="64px" height="64px">
                                    <strong>{{$post->user->name}}</strong>
                                </div>
                                <div class="card-body">
                                    <a class="btn" href="{{route('show',['id' => $post->id])}}">
                                        <img class="img-thumbnail" style="display: block; margin: auto;" src="{{url("storage/img/$post->photo")}}">
                                    </a>
                                    <p><small>uploaded: {{$post->created_at}}</small></p>
                                    <p><small>{{count($post->likes)}} likes</small></p>
                                    @if(count($post->comments) > 0)
                                        @foreach($post->comments as $comment)
                                            <p>
                                                <img class="rounded-circle mr-2" src="{{url('storage/img/'.$comment->user->profile->photo)}}" width="32px" height="32px">
                                                <strong>{{$comment->user->name}}</strong><br>
                                                <small>{{$comment->content}}</small>
                                            </p>
                                        @endforeach
                                    @else
                                        <p class="alert alert-danger">No Commnets</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                @endforeach
            @else
                <p class="mt-4 alert alert-danger">You dont have any followers</p>
            @endif
        </div>
    </div>
@endsection