@extends('layouts.app2')

@section('content')
    <div class="card mt-4">
        <div class="card-header">
            <div class="row mt-4 justify-content-center">
                <div class="col-md-2">
                    <img class="rounded-circle" src="{{url('storage/img/'.$user->profile->photo)}}" width="128px" height="128px">
                </div>
                <div class="col-md-4">
                    <div class="row text-center">
                        <div class="col mt-4">
                            <strong><p>{{count($user->posts)}}</p><p>Posts</p></strong>
                        </div>
                        <div class="col mt-4">
                            <strong> <p id="follower">{{count($user->follower)}}</p><p>Followers</p></strong>
                        </div>
                        <div class="col mt-4">
                            <strong><p>{{count($user->followed)}}</p><p>Following</p></strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            @if(Auth::user()->id == $user->id)
                                <a href="{{route('edit')}}" class="btn btn-outline-primary btn-block">Edit Profile</a>
                            @else
                                <button id="{{$user->id}}" class="follow btn btn-outline-primary btn-block"></button>
                            @endif
                            <p class="mt-2">
                                @include('layouts.message')
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 justify-content-center">
                <div class="col-md-6">
                    <p>
                        <img src="{{url('img/'.$user->profile->country->photo)}}" width="32" height="32">
                        <strong>{{$user->profile->username}} <small>({{$user->profile->gender}})</small></strong><br>
                        <small>Telephone: {{$user->profile->telephone}}</small>
                    </p>
                    <p>{{$user->profile->bio}}</p>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col text-center">
                    @if(count($user->posts) > 0)
                        @foreach($user->posts as $post)
                            <a class="btn" href="{{route('show',['id' => $post->id])}}">
                                <img class="fit mb-1" src="{{url('storage/img/'.$post->photo)}}" width="250px" height="250px">
                            </a>
                        @endforeach
                    @else
                        <p class="alert alert-danger">No Posts Available</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/follow.js') }}"></script>
@endsection