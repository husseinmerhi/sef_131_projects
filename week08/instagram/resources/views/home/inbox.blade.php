@extends('layouts.app2')

@section('content')
    <div class="row mt-4 justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4>My Inbox</h4>
                </div>
                <div class="card-body">
                    @include('layouts.message')
                    <form method="POST" action="{{route('message')}}">
                        @csrf
                        <div class="form-group">
                            <select class="form-control" name="receiver">
                                @if(count($receivers) > 0)
                                    @foreach($receivers as $receiver)
                                        <option value="{{$receiver->userFollowed->id}}">{{$receiver->userFollowed->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="content" rows="5" placeholder="Your Message ..."></textarea>
                        </div>
                        <input type="submit" class="btn btn-primary btn-block" value="Send">
                    </form>
                    <hr>
                    @if(count($user->receiver) > 0)
                        @foreach($user->receiver as $message)
                            <strong><p>{{$message->userSender->name}}</p></strong>
                            <p>{{$message->content}}</p>
                            <hr>
                        @endforeach
                    @else
                        <p class="mt-4 alert alert-danger">You dont have any messages!</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4>My Messages</h4>
                </div>
                <div class="card-body">
                    @if(count($user->sender) > 0)
                        @foreach($user->sender as $message)
                            <strong><p>{{$message->userReceived->name}}</p></strong>
                            <p>{{$message->content}}</p>
                            <hr>
                        @endforeach
                    @else
                        <p class="mt-4 alert alert-danger">You didnt send any messages!</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection