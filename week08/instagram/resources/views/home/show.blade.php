@extends('layouts.app2')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="result"></div>
            <div class="card mt-4">
                <div class="card-header">
                    @if(Auth::user()->id == $post->user->id)
                        <form method="post" action="{{route('deletePost')}}">
                            @csrf
                            <input type="text" name="post" value="{{$post->id}}" hidden>
                            <input class="delete btn btn-danger float-right" type="submit" value="x">
                        </form>
                    @endif
                    <img class="rounded-circle mr-4" src="{{url('storage/img/'.$post->user->profile->photo)}}" width="64px" height="64px">
                    <strong>{{$post->user->name}}</strong>
                </div>
                <div class="card-body">
                    @include('layouts.message')
                    <img class="img-thumbnail" style="display: block; margin: auto;" src="{{url("storage/img/$post->photo")}}">
                    <p><small>uploaded: {{$post->created_at}}</small></p>
                    <button id="{{$post->id}}" class="like btn btn-outline-danger p-1 mb-4">&hearts; {{count($post->likes)}}</button>
                    @if(count($post->comments) > 0)
                        @foreach($post->comments as $comment)
                            <p>
                                @if(Auth::user()->id == $comment->user->id)
                                    <form method="post" action="{{route('deleteComment')}}">
                                        @csrf
                                        <input type="text" name="comment" value="{{$comment->id}}" hidden>
                                        <input class="delete btn btn-danger float-right" type="submit" value="x">
                                    </form>
                                @endif
                                <img class="rounded-circle mr-2" src="{{url('storage/img/'.$comment->user->profile->photo)}}" width="32px" height="32px">
                                <strong>{{$comment->user->name}}</strong><br>
                                <small>{{$comment->content}}</small>
                            </p>
                            <hr>
                        @endforeach
                    @else
                        <p class="alert alert-danger">No Commnets</p>
                    @endif
                    <form method="POST" action="{{route('comment')}}">
                        @csrf
                        <div class="form-group">
                            <input type="text" value="{{$post->id}}" name="post" hidden>
                            <textarea class="form-control" name="comment" placeholder="Write Your Comment ... "></textarea>
                        </div>
                        <input class="btn btn-primary btn-block" type="submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/like.js') }}"></script>
@endsection