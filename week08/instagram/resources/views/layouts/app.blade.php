<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <title>{{config('app.name')}}</title>
</head>
<body>
<div class="container">
    <div class="row justify-content-center push-top">
        <div class="col-sm-5">
            <div class="card">
                <div class="card-body">
                   @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
