@extends('layouts.app')
@section('content')
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group">
            <h4 class="billabong text-center">Instagram</h4>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
            @endif
        </div>

        <div class="form-group">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-primary btn-block">
                {{ __('Login') }}
            </button>
        </div>
        <hr class="hr-text" data-content="OR">
        <div class="form-group text-center">
            don't have an account? <a class="btn btn-link" href="{{route('register')}}">Sign Up</a>
        </div>
    </form>
@endsection