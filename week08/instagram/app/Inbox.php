<?php

namespace instagram;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    public function userReceived(){
        return $this->belongsTo('instagram\User', 'receiver_id');
    }

    public function userSender(){
        return $this->belongsTo('instagram\User', 'sender_id');
    }
}
