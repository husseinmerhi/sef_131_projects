<?php

namespace instagram;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    public function userFollowed(){
        return $this->belongsTo('instagram\User', 'followed_id');
    }

    public function userFollower(){
        return $this->belongsTo('instagram\User', 'follower_id');
    }
}
