<?php

namespace instagram;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function country(){
        return $this->belongsTo('instagram\Country');
    }
}
