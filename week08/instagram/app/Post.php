<?php

namespace instagram;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user(){
        return $this->belongsTo('instagram\User');
    }

    public function comments(){
        return $this->hasMany('instagram\Comment');
    }

    public function likes(){
        return $this->hasMany('instagram\Like');
}
}
