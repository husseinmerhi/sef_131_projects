<?php

namespace instagram;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile(){
        return $this->hasOne('instagram\Profile');
    }

    public function followed(){
        return $this->hasMany('instagram\Follow', 'follower_id');
    }

    public function follower(){
        return $this->hasMany('instagram\Follow', 'followed_id');
    }

    public function posts(){
        return $this->hasMany('instagram\Post');
    }

    public function sender(){
        return $this->hasMany('instagram\Inbox', 'sender_id');
    }

    public function receiver(){
        return $this->hasMany('instagram\Inbox', 'receiver_id');
    }
}
