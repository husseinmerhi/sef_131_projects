<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use instagram\Post;
use instagram\Comment;
use instagram\Like;

class PostController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        return view('home/post');
    }

    public function submit(Request $request){
        $this->validate($request, [
            'photo' => 'required|image|mimes:jpg,jpeg,png',
            'caption' => 'required|max:500'
        ]);

        //handle file upload
        if($request->hasFile('photo')){

            //get filename with the extension
            $filenameWithExt = $request->file('photo')->getClientOriginalName();

            //get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            //get just extension
            $extension = $request->file('photo')->getClientOriginalExtension();

            //filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            //upload image
            $path = $request->file('photo')->storeAs('public/img', $fileNameToStore);
        }

        $post = new Post();
        $post->user_id = Auth::user()->id;
        $post->photo = $fileNameToStore;
        $post->caption = $request->input('caption');

        $post->save();

        return redirect('/post')->with('success', 'Post Uploaded!');
    }

    public function show($id){
        $post = POST::find($id);
        $id = Auth::user()->id;
        return view('home/show')->with(['post' => $post, 'id' => $id]);
    }

    public function comment(Request $request){
        $this->validate($request, [
            'comment' => 'required|max:500'
        ]);

        $comment = new Comment();
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $request->input('post');
        $comment->content = $request->input('comment');

        $comment->save();

        return redirect('/post/'.$request->input('post'))->with('success', 'Comment Added!');
    }

    public function deleteComment(Request $request) {
        Comment::find($request->input('comment'))->delete();
        return redirect()->back()->with('success', 'Comment Deleted!');
    }

    public function deletePost(Request $request) {
        //delete all related likes
        $likes = Like::where('post_id', $request->input('post'))->get();
        if (count($likes) > 0) {
            foreach ($likes as $like) {
                $like->delete();
            }
        }
        //delete all related commenets
        $comments = Comment::where('post_id', $request->input('post'))->get();
        if (count($comments) > 0) {
           foreach ($comments as $comment) {
               $comment->delete();
           }
        }
        //delete post
        Post::find($request->input('post'))->delete();
        return redirect('/profile')->with('success', 'Post Deleted!');
    }
}
