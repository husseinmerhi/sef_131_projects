<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use instagram\Profile;
use instagram\Country;

class EditController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $user = Auth::user();
        $countries = Country::all();
        return view('home/edit')->with(['user' => $user, 'countries' => $countries]);
    }

    public function update(Request $request){
        $this->validate($request, [
            'username' => 'max:100',
            'telephone' => 'max:12',
            'gender' => '',
            'country' => '',
            'bio' => 'max:500',
            'photo' => 'image|mimes:jpg,jpeg,png|nullable'
        ]);

        //handle file upload
        if($request->hasFile('photo')){

            //get filename with the extension
            $filenameWithExt = $request->file('photo')->getClientOriginalName();

            //get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            //get just extension
            $extension = $request->file('photo')->getClientOriginalExtension();

            //filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            //upload image
            $path = $request->file('photo')->storeAs('public/img', $fileNameToStore);
        }
        else {
            $fileNameToStore = 'noimage.jpg';
        }

        //update profile
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        if($request->input('username')){
            $profile->username = $request->input('username');
        }
        if($request->input('telephone')){
            $profile->telephone = $request->input('telephone');
        }
        if($request->input('gender')){
            $profile->gender = $request->input('gender');
        }
        if($request->input('country')){
            $profile->country_id = $request->input('country');
        }
        if($request->input('bio')){
            $profile->bio = $request->input('bio');
        }
        if($fileNameToStore != 'noimage.jpg'){
            $profile->photo = $fileNameToStore;
        }

        $profile->save();

        return redirect('/edit')->with('success', 'Profile Updated!');
    }
}
