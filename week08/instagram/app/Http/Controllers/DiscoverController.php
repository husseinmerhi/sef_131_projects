<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;
use instagram\User;
use instagram\Profile;
use Illuminate\Support\Facades\Auth;

class DiscoverController extends Controller
{
    public function index(){
        $users = User::where('id', "!=", Auth::user()->id)->orderBy('created_at','desc')->paginate(5);
        return view('home/discover')->with('users', $users);
    }
}
