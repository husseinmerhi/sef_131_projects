<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use instagram\User;
use instagram\Profile;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $user = Auth::user();
        if (count(Profile::where('user_id', $user->id)->get()) > 0) {
            return view('home/profile')->with('user', $user);
        }
        else {
            $profile = new Profile();
            $profile->user_id = $user->id;
            $profile->username = $user->name;
            $profile->photo = 'default.png';
            $profile->country_id = 1;
            $profile->telephone = 'xx-xxxxxx';
            $profile->bio = 'this is my bio';
            $profile->save();
            return view('home/profile')->with('user', $user);
        }

    }

    public function display($id){
        $user = User::find($id);
        return view('/home/profile')->with('user', $user);
    }
}
