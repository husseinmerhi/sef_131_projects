<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use instagram\Like;
use instagram\Follow;

class AjaxController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function like(Request $request) {
        $result = Like::where('user_id', Auth::user()->id)->where('post_id', $request->input('post'))->first();
        if (count($result) > 0) {
            $result->delete();
            return 'unlike';
        }
        else {
            $like = new Like();
            $like->user_id = Auth::user()->id;
            $like->post_id = $request->input('post');
            $like->save();
            return 'like';
        }
    }

    public function follow(Request $request){
        $result = Follow::where('follower_id', Auth::user()->id)
                            ->where('followed_id', $request->input('target'))->first();
        if (count($result) > 0) {
            $result->delete();
            return 'unfollow';
        }
        else {
            $follow = new Follow();
            $follow->follower_id = Auth::user()->id;
            $follow->followed_id = $request->input('target');
            $follow->save();
            return 'follow';
        }
    }

    public function update(Request $request){
        $result = Follow::where('followed_id', $request->input('target'))->get();
        $exist = Follow::where('followed_id', $request->input('target'))
                        ->where('follower_id', Auth::user()->id)->first();
        if (count($exist) > 0 ) {
            $status = 'Unfollow';
        }
        else {
            $status = 'Follow';
        }
        return count($result).','.$status;
    }
}
