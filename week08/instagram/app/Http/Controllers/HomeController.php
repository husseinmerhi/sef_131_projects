<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;
use instagram\Post;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('home/home')->with('user', $user);
    }
}
