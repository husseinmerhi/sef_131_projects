<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use instagram\Inbox;

class InboxController extends Controller
{
    public function index(){
        $user = Auth::user();
        $receivers = $user->followed;
        return view('/home/inbox')->with(['user' => $user, 'receivers' => $receivers]);
    }

    public function send(Request $request){
        $this->validate($request, [
            'content' => 'required|max:500'
        ]);

        $message = new Inbox();
        $message->sender_id = Auth::user()->id;
        $message->receiver_id = $request->input('receiver');
        $message->content = $request->input('content');

        $message->save();

        return redirect()->back()->with('success', 'Message Sent!');
    }
}
