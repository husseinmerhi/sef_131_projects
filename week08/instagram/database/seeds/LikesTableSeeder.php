<?php

use Illuminate\Database\Seeder;
use instagram\Like;

class LikesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        $like = new Like();
        $like->user_id = 2;
        $like->post_id = 1;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 3;
        $like->post_id = 1;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 1;
        $like->post_id = 2;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 3;
        $like->post_id = 2;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 1;
        $like->post_id = 3;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 5;
        $like->post_id = 3;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 2;
        $like->post_id = 4;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 3;
        $like->post_id = 4;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 3;
        $like->post_id = 5;
        $like->save();
        //
        $like = new Like();
        $like->user_id = 1;
        $like->post_id = 5;
        $like->save();
    }
}
