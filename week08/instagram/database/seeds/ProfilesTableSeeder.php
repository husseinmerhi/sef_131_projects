<?php

use Illuminate\Database\Seeder;
use instagram\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        $profile = new Profile();
        $profile->user_id = 1;
        $profile->username = 'Hussein';
        $profile->country_id = 1;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'male';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 2;
        $profile->username = 'Ali';
        $profile->country_id = 2;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'male';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 3;
        $profile->username = 'Mohamad';
        $profile->country_id = 3;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'male';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 4;
        $profile->username = 'Hassan';
        $profile->country_id = 4;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'male';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 5;
        $profile->username = 'Fatima';
        $profile->country_id = 5;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'female';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 6;
        $profile->username = 'atieh';
        $profile->country_id = 6;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'male';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 7;
        $profile->username = 'zreik';
        $profile->country_id = 7;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'male';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 8;
        $profile->username = 'zahraa';
        $profile->country_id = 8;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'female';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 9;
        $profile->username = 'wahid';
        $profile->country_id = 9;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'male';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
        //
        $profile = new Profile();
        $profile->user_id = 10;
        $profile->username = 'Mayushii';
        $profile->country_id = 1;
        $profile->telephone = 'xx-xxxxxx';
        $profile->gender = 'female';
        $profile->bio = 'this is my bio';
        $profile->photo = 'defaultA.png';
        $profile->save();
    }
}
