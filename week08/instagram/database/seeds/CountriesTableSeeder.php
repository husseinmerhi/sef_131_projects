<?php

use Illuminate\Database\Seeder;
use instagram\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        $country = new Country();
        $country->name = 'Lebanon';
        $country->photo = 'lebanon.png';
        $country->save();
        //
        $country = new Country();
        $country->name = 'Syria';
        $country->photo = 'syria.png';
        $country->save();
        //
        $country = new Country();
        $country->name = 'Palestine';
        $country->photo = 'palestine.png';
        $country->save();
        //
        $country = new Country();
        $country->name = 'Iraq';
        $country->photo = 'iraq.png';
        $country->save();
        //
        //
        $country = new Country();
        $country->name = 'Yemen';
        $country->photo = 'yemen.png';
        $country->save();
        //
        $country = new Country();
        $country->name = 'Iran';
        $country->photo = 'iran.png';
        $country->save();
        //
        $country = new Country();
        $country->name = 'Russia';
        $country->photo = 'ruissa.png';
        $country->save();
        //
        $country = new Country();
        $country->name = 'China';
        $country->photo = 'china.jpeg';
        $country->save();
        //
        $country = new Country();
        $country->name = 'Korea';
        $country->photo = 'korea.jpeg';
        $country->save();

    }
}
