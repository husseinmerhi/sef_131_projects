<?php

use Illuminate\Database\Seeder;
use instagram\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        $comment = new Comment();
        $comment->user_id = 2;
        $comment->post_id = 1;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 3;
        $comment->post_id = 1;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 5;
        $comment->post_id = 2;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 4;
        $comment->post_id = 2;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 2;
        $comment->post_id = 3;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 4;
        $comment->post_id = 3;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 5;
        $comment->post_id = 4;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 1;
        $comment->post_id = 4;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 3;
        $comment->post_id = 5;
        $comment->content = 'this is my comment';
        $comment->save();
        //
        $comment = new Comment();
        $comment->user_id = 4;
        $comment->post_id = 5;
        $comment->content = 'this is my comment';
        $comment->save();
    }
}
