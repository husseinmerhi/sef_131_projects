<?php

use Illuminate\Database\Seeder;
use instagram\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        $post = new Post();
        $post->user_id = 1;
        $post->photo = 'defaultP.png';
        $post->caption = 'this is my post';
        $post->save();
        //
        $post = new Post();
        $post->user_id = 2;
        $post->photo = 'defaultP.png';
        $post->caption = 'this is my post';
        $post->save();
        //
        $post = new Post();
        $post->user_id = 3;
        $post->photo = 'defaultP.png';
        $post->caption = 'this is my post';
        $post->save();
        //
        $post = new Post();
        $post->user_id = 4;
        $post->photo = 'defaultP.png';
        $post->caption = 'this is my post';
        $post->save();
        //
        $post = new Post();
        $post->user_id = 5;
        $post->photo = 'defaultP.png';
        $post->caption = 'this is my post';
        $post->save();
    }
}
