<?php

use Illuminate\Database\Seeder;
use instagram\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User();
        $user->name = 'hussein merhi';
        $user->email = 'husseinmerhi@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'ali merhi';
        $user->email = 'alimerhi@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'mohamad merhi';
        $user->email = 'mohamadmerhi@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'hassan merhi';
        $user->email = 'hassanmerhi@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'fatima merhi';
        $user->email = 'fatimamerhi@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'ali atieh';
        $user->email = 'ali@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'ali zreik';
        $user->email = 'zreik@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'zahraa haydar';
        $user->email = 'zahraa@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'wahid fattel';
        $user->email = 'wahid@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
        //
        $user = new User();
        $user->name = 'Mayushi';
        $user->email = 'mayushi@hotmail.com';
        $user->password = Hash::make('admin@123');
        $user->status = 'active';
        $user->save();
    }
}
