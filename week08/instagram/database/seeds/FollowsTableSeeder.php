<?php

use Illuminate\Database\Seeder;
use instagram\Follow;

class FollowsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        $follow = new Follow();
        $follow->follower_id = 2;
        $follow->followed_id = 1;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 3;
        $follow->followed_id = 1;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 1;
        $follow->followed_id = 2;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 3;
        $follow->followed_id = 2;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 1;
        $follow->followed_id = 3;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 5;
        $follow->followed_id = 3;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 2;
        $follow->followed_id = 4;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 3;
        $follow->followed_id = 4;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 3;
        $follow->followed_id = 5;
        $follow->save();
        //
        $follow = new Follow();
        $follow->follower_id = 1;
        $follow->followed_id = 5;
        $follow->save();
    }
}
