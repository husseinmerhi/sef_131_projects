<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthenticationController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'ProfileController@index')->name('profile');

Route::get('/profile/{id}', 'ProfileController@display')->name('view');

Route::post('/profile/follow', "AjaxController@follow");

Route::post('/profile/update', 'AjaxController@update');

Route::get('/edit', 'EditController@index')->name('edit');

Route::post('/edit', 'EditController@update')->name('update');

Route::get('/post', 'PostController@index')->name('post');

Route::post('/post', 'PostController@submit')->name('submit');

Route::get('/post/{id}', 'PostController@show')->name('show');

Route::post('/delete', 'PostController@deletePost')->name('deletePost');

Route::post('/post/like', 'AjaxController@like');

Route::post('/comment', 'PostController@comment')->name('comment');

Route::post('/post/delete', 'PostController@deleteComment')->name('deleteComment');

Route::get('/discover', 'DiscoverController@index')->name('discover');

Route::post('/search', 'DiscoverController@search');

Route::get('/inbox', 'InboxController@index')->name('inbox');

Route::post('/send', 'InboxController@send')->name('message');