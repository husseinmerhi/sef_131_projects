var pillarSource = ['8', '7', '6', '5', '4', '3', '2', '1'];
var pillarTarget = [];
var pillarVia = [];
var diskHeight = 10;
var diskShift = 320;
var stackSteps = [];

function play() {
    TowerOfHanoi(8, '1', '2', '3');
    Animation();
}

function TowerOfHanoi(n, source, via, target) {
    if (n > 0) {
        TowerOfHanoi(n - 1, source, target, via);
        stackSteps.push([source, target]);
        TowerOfHanoi(n - 1, via, source, target);
    }
}

function Animation() {
    var fromTo = [];
    var disk;
    var elem;
    var disks;
    var doneTop = false;
    var doneLeft = false;

    if (stackSteps.length > 0) {
        //extract from the stack
        fromTo = stackSteps.shift();
        from = fromTo[0];
        to = fromTo[1];

        //get disk from source pillar
        switch (from) {
            case '1':
                disk = pillarSource.pop();
                break;
            case '2':
                disk = pillarVia.pop();
                break;
            case '3':
                disk = pillarTarget.pop();
                break;
        }

        //get the div represent the disk
        switch (disk) {
            case '1':
                elem = document.getElementById('disk1');
                break;
            case '2':
                elem = document.getElementById('disk2');
                break;
            case '3':
                elem = document.getElementById('disk3');
                break;
            case '4':
                elem = document.getElementById('disk4');
                break;
            case '5':
                elem = document.getElementById('disk5');
                break;
            case '6':
                elem = document.getElementById('disk6');
                break;
            case '7':
                elem = document.getElementById('disk7');
                break;
            case '8':
                elem = document.getElementById('disk8');
                break;
        }

        //get number of disks in target pillar
        switch (to) {
            case '1':
                disks = pillarSource.length + 1;
                break;
            case '2':
                disks = pillarVia.length + 1;
                break;
            case '3':
                disks = pillarTarget.length + 1;
                break;
        }

        //get new top position
        var box = document.getElementsByClassName('box')[0];
        var boxBottom = box.offsetTop + box.offsetHeight;
        var topPostion = boxBottom - disks * diskHeight;

        //get new left position abd direction
        var leftPostion = elem.offsetLeft + ((to - from) * diskShift);

        //change disk position
        changeTop(elem, topPostion);
        changeLeft(elem, leftPostion);
        //elem.style.top = topPostion;
        //elem.style.left = leftPostion;

        //add disk to new pillar stack
        switch (to) {
            case '1':
                pillarSource.push(disk);
                break;
            case '2':
                pillarVia.push(disk);
                break;
            case '3':
                pillarTarget.push(disk);
                break;
        }
    }
}

function changeTop(elem, topPostion) {
    var interval = setInterval(up, 1);
    function up() {
        if (elem.offsetTop == topPostion) {
            clearInterval(interval);
        }
        else {
            if (elem.offsetTop > topPostion) {
                elem.style.top = (elem.offsetTop - 1) + 'px';
            }
            else {
                elem.style.top = (elem.offsetTop + 1) + 'px';
            }
        }
    }
}

function changeLeft(elem, leftPostion) {
    var interval = setInterval(side, 1);
    function side() {
        if (elem.offsetLeft == leftPostion) {
            clearInterval(interval);
            Animation();
        }
        else {
            if (elem.offsetLeft > leftPostion) {
                elem.style.left = (elem.offsetLeft - 1) + 'px';
            }
            else {
                elem.style.left = (elem.offsetLeft + 1) + 'px';
            }
        }
    }
}

function reset() {
    location.reload();
}