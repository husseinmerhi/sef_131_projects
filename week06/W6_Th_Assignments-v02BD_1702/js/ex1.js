var charlist = "abcdefghijklmnopqrstuvwxyz";

function checkPass(password) {
    var total = 0;
    for (var i = 0; i < password.length; i++) {
        var countone = password.charAt(i);
        var counttwo = (charlist.indexOf(countone));
        counttwo++;
        total *= 17;
        total += counttwo;
    }
    if (total == 248410397744610) {
        console.log('solved');
    } else {
        alert('wrong password try again later loser!');
    }
}

function hack(total, password) {
    if (total == 0) {
        console.log(reverseString(password));
        checkPass(reverseString(password));
    }
    else {
        for (var i = 0; i < 26; i++) {
            if ((total - (i + 1)) % 17 == 0) {
                total = (total - (i + 1)) / 17;
                password += charlist.charAt(i);
                hack(total, password);
                break;
            }
        }
    }
}

function reverseString(str) {
    var splitString = str.split("");
    var reverseArray = splitString.reverse();
    var joinArray = reverseArray.join("");
    return joinArray;
}

hack(248410397744610, '');