window.onload = function () {

    //if browser support local storage
    if (localStorage !== 'undefiend') {
        var interval = setInterval(showDate, 1000);
        init();
        action();
    }
    //if browser doesnt support local storage
    else {
        var p = document.createElement("p");
        var text = document.createTextNode("Your browser doesnt support local storage");
        p.appendChild(text);
        var a1 = document.createElement("a");
        a1.href = "https://www.google.com/chrome/";
        a1.innerHTML = "Google Chrome";
        var a2 = document.createElement("a");
        a2.href = "https://www.mozilla.org/en-US/firefox/new/";
        a2.innerHTML = "Mozilla FireFox";
        var container = document.querySelector(".container");
        container.appendChild(p);
        container.appendChild(a1);
        container.appendChild(a2);
    }
}

function init() {
    showDate();
    if (!localStorage.length == 0) {
        var timestamps = [];
        for (var i = 0; i < localStorage.length; i++) {
            var key = localStorage.key(i);
            timestamps.push(key);
        }
        timestamps.sort();
        for (var i = 0; i < timestamps.length; i++) {
            var data = localStorage.getItem(timestamps[i]);
            createNode(key, data);
        }
    }
}

function action() {

    document.getElementsByClassName("container")[0].addEventListener("click", function (event) {
        if (event.target.className == "insertButton") {
            var element = document.getElementsByClassName("insert")[0];
            if (element.offsetHeight == 0) {
                event.target.innerHTML = "-";
                show(element,event.target);
            }
            else {
                event.target.innerHTML = "+";
                hide(element,event.target);
            }
        }
        else if (event.target.id == "add") {
            //get information
            var title = document.getElementById("title").value;
            var description = document.getElementById("description").value;
            if (/\S+/.test(title) && /\S+/.test(description)) {
                //save them
                var date = new Date();
                var obj = {title:title,description:description,date:date.toDateString()+ ' ' + date.toTimeString()};
                var data = JSON.stringify(obj);
                localStorage.setItem(date.getTime(),data);
                //add node
                createNode(date.getTime(),data);
                document.getElementById("title").value = '';
                document.getElementById("description").value = '';
            }
            else {
                alert('both fields are required!');
            }
        }
        else if (event.target.className == "delete") {
            localStorage.removeItem(localStorage.getItem(event.target.id));
            localStorage.removeItem(event.target.id);
            event.target.parentElement.remove();
        }
    });
}

function createNode(timeStamp, data) {
    var node = document.createElement("div");
    node.className = "node";
    var obj = JSON.parse(data);
    var titleElem = document.createElement("h4");
    titleElem.innerHTML = obj.title;
    var dateElem = document.createElement("p");
    dateElem.className = 'small';
    dateElem.innerHTML = obj.date;
    var descriptionElem = document.createElement("p");
    descriptionElem.innerHTML = obj.description;
    var button = document.createElement("button");
    button.className = "delete";
    button.id = timeStamp;
    node.appendChild(titleElem);
    node.appendChild(dateElem);
    node.appendChild(descriptionElem);
    node.appendChild(button);
    var list = document.getElementsByClassName("list")[0];
    list.insertBefore(node, list.firstChild);
}

function show(element,target) {
    var interval = setInterval(down, 1);
    function down() {
        if (element.offsetHeight == 210) {
            document.getElementById("title").focus();
            clearInterval(interval);
        }
        else {
            element.style.height = (parseInt(element.offsetHeight) + 1) + 'px';
        }
    }
}

function hide(element) {
    var interval = setInterval(up, 1);
    function up() {
        if (element.offsetHeight == 0) {
            clearInterval(interval);
        }
        else {
            element.style.height = (parseInt(element.offsetHeight) - 1) + 'px';
        }
    }
}

function showDate() {
    var date = new Date();
    var elem = document.getElementById('date');
    elem.innerHTML = date.toDateString() + "<br>" + date.toLocaleTimeString();
}