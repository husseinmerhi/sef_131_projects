<?php require_once('Design.php'); $design = new Design(); session_start(); ?>
<!DOCTYPE hmtl>
<html>
    <head>
        <title>Order</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <form method="post" action="order_process.php">
            <fieldset>
                <legend>Place Your Order</legend>
                <table>
                    <tr><td>Email</td><td><input type="email" name="email"></td></tr>
                    <tr>
                        <td>Film</td>
                        <td>
                            <select name="film">
                                <?php $design->film_menu(); ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="submit" value="submit"></td>
                        <td>
                            <?php
                                if(isset($_SESSION['response']) && !empty($_SESSION['response'])) {
                                    echo $_SESSION['response'];
                                 }
                            ?>
                        </td>
                    </tr>
            </fieldset>
        </form>
    </body>
</html>