<?php
    require_once('config.php');
    class Query {
        public $link;
        public function execute($sql) {
            $this->link = new mysqli(HOST,USER,PASS,DATABASE);
            $result = $this->link->query($sql);
            return $result;
        }
    }
?>