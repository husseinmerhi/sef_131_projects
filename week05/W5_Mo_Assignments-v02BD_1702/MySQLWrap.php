<?php
    
    require_once('Query.php');
    
    class MySQLWrap {

        public function check_email($email) {
            $sql = "select customer_id from customer where email= '$email'";
            $query = new Query();
            $result = $query->execute($sql);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                return $row['customer_id'];
            }
            else {
                return false;
            }
        }

        public function check_film_in_store($customer_id,$film_id) {
            $sql = "select inventory_id from inventory
                    where film_id = '$film_id'
                    and store_id IN (
                        select store_id from customer where customer_id = '$customer_id'
                    )
                    and inventory_id NOT IN (
                        select i.inventory_id from inventory as i
                        join rental as r on i.inventory_id = r.inventory_id
                        where film_id = '$film_id'
                        and r.return_date is null
                        and store_id IN (
                            select store_id from customer where customer_id = '$customer_id'
                        )
                    );";
            $query = new Query();
            $result = $query->execute($sql);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                return $row['inventory_id'];
            }
            else {
                return false;
            }
        }

        public function get_staff_id($customer_id) {
            $sql = "select manager_staff_id from store as s
                    join customer as c on c.store_id = s.store_id
                    and customer_id = '$customer_id'";
            $query = new Query();
            $result = $query->execute($sql);
            $row = $result->fetch_assoc();
            return $row['manager_staff_id'];
        }

        public function insert_rental($customer_id,$inventory_id,$staff_id) {
            $sql = "insert into rental(rental_date, inventory_id, customer_id, staff_id)
                    values (NOW(),'$inventory_id','$customer_id','$staff_id');";
            $query = new Query();
            $result = $query->execute($sql);
            if ($result) {
                return true;
            }
            else {
                return false;
            }
        }
    }
?>