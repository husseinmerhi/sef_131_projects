<?php
    session_start();
    require_once('MySQLWrap.php');
    $wrap = new MySQLWrap();
    //check email
    $customer_id = $wrap->check_email($_POST['email']);
    if ($customer_id > 0) {
        //check film
        $inventory_id = $wrap->check_film_in_store($customer_id,$_POST['film']);
        if ($inventory_id > 0) {
            //get staff_id
            $staff_id = $wrap->get_staff_id($customer_id);
            //insert rental
            $wrap->insert_rental($customer_id,$inventory_id,$staff_id);
            $_SESSION['response'] = "success: inventory_id $inventory_id has been rented";
            header('location:order.php'); 
        }
        else {
            $_SESSION['response'] = 'error: no quantity available';
            header('location:order.php');
        }
    }
    else {
        $_SESSION['response'] = 'error: email doesnt exist';
        header('location:order.php');
    }
?>