window.onload = function() {
    document.getElementById("url").value = "https://medium.com/s/story/how-compassion-guided-me-up-the-mountains-of-india-e4e7895a671d";
    document.getElementById("button").addEventListener("click", function(event) {
        document.getElementById("loader").style.display = 'block';
        getData();
    });
};

function getData() {
    var url = document.getElementById("url").value;
    var sentences = document.getElementById("sentences").value;
    var data = "option=retrieve&url=" + url;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "request.php", true);
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.send(data);
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var html = new DOMParser().parseFromString(this.responseText, "text/html");
            var title = html.getElementsByTagName("h1")[0].textContent;
            var paragraph = html.getElementsByClassName('section-inner')[0];
            var text = '';
            for (var i = 0; i < paragraph.childElementCount; i++) {
                if (paragraph.childNodes[i].tagName == "P") {
                    text += paragraph.childNodes[i].textContent;
                }
            }
            getSummary(title,text,sentences);
        }
    };
}

function getSummary(title,text,sentences) {
    var data = "option=summerize&title=" + title + '&text=' + text + "&sentences=" + sentences;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "request.php", true);
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.send(data);
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("loader").style.display = 'none';
            var data = JSON.parse(this.responseText);
            var result = document.getElementById('result');
            while (result.firstChild) {
                result.removeChild(result.firstChild);
            }
            var h1 = document.createElement('h1');
            h1.innerHTML = title;
            result.appendChild(h1);
            for (var i = 0; i < data.sentences.length; i++) {
                var p = document.createElement('P');
                p.innerHTML += data.sentences[i];
                result.appendChild(p);
            }
        }
    };
}