<?php
    switch($_POST['option']){
        case 'retrieve':
            $httpRequest = curl_init();
            curl_setopt($httpRequest, CURLOPT_URL, $_POST['url']);
            curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
            echo curl_exec($httpRequest);
            curl_close($httpRequest);
            break;
        case 'summerize':
            $url = 'https://api.aylien.com/api/v1/summarize';
            $header = array(
                'Accept: application/json',
                'Content-Type: application/x-www-form-urlencoded',
                'X-AYLIEN-TextAPI-Application-ID:' . "e3e6c53e",
                'X-AYLIEN-TextAPI-Application-Key:' . "6215514211a440c7e53b0b6ceab5de33"
            );
            $body = array(
                'title' => $_POST['title'],
                'text' => $_POST['text'],
                'sentences_number' => $_POST['sentences']
            );
            $httpRequest = curl_init($url);
            curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($httpRequest, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($httpRequest, CURLOPT_HTTPHEADER, $header);
            curl_setopt($httpRequest, CURLOPT_POSTFIELDS,
                preg_replace("/%5B[0-9]+%5D=/i", "=", http_build_query($body)));
            echo curl_exec($httpRequest);
            curl_close($httpRequest);
            break;
    }
?>