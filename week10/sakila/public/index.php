<?php

    require_once('../app/config/config.php');

    spl_autoload_register(function ($class) {
        include ROOT . $class . '.php';
        include APP . $class . '.php';
        include KERNEL . $class . '.php';
        include CONTROLLER . $class . '.php';
        include MODEL . $class . '.php';
        include VIEW . $class . '.php';
    });

    new Router;