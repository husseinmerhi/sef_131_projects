<?php

class ErrorController extends Controller {

    public function notFound() {
        $this->view('404');
        $this->view->setTitle('Error');
        $this->view->render();
    }

    public function notAllowed() {
        $this->view('405');
        $this->view->setTitle('Error');
        $this->view->render();
    }

}