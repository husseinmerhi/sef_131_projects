<?php

class ActorController extends APIController {

    public function delete($model, $method, $query) {
        if ($method == 'DELETE') {
            if ($query != '') {
                parse_str($query, $data);
                if (is_numeric($data['id'])) {
                    $modelInstance = new $model(strtolower($model));
                    //delete dependencies
                    $feedback = $modelInstance->delete($data['id']);
                    //delete target
                    $feedback = $modelInstance->delete($data['id']);
                    if ($feedback['status'] == 'success') {
                        $response = new Response(json_encode($feedback), 200);
                        $response->send();
                    } else {
                        $response = new Response(json_encode($feedback), 500);
                        $response->send();
                    }
                }
                else {
                    $feedback['status'] = 'failed';
                    $feedback['content'] = 'id should be numeric';
                    $response = new Response(json_encode($feedback), 400);
                    $response->send();
                }

            }
            else {
                $feedback['status'] = 'failed';
                $feedback['content'] = 'you have entered an empty query';
                $response = new Response(json_encode($feedback), 400);
                $response->send();
            }
        }
        else {
            $feedback['status'] = 'failed';
            $feedback['content'] = 'you have to use DELETE in delete statement';
            $response = new Response(json_encode($feedback), 405);
            $response->send();
        }
    }
}