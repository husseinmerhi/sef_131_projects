<?php

class HomeController extends Controller {

    public function index() {
        $this->view('home');
        $this->view->setTitle('Home');
        $this->view->render();
    }

    public function schema() {
        $this->view('schema');
        $this->view->setTitle('Schema');
        $this->view->render();
    }

    public function contact() {
        $this->view('contact');
        $this->view->setTitle('Contact Us');
        $this->view->render();
    }

}