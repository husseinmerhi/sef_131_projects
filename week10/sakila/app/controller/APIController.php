<?php

class APIController extends Controller {

    public function find($model, $method, $query) {
        if ($method == 'GET') {
            if ($query != '') {
                parse_str($query, $data);
                if (is_numeric($data['id'])) {
                    $modelInstance = new $model(strtolower($model));
                    $feedback = $modelInstance->find($data['id']);
                    if ($feedback['status'] == 'success' || $feedback['status'] == 'warning') {
                        $response = new Response(json_encode($feedback), 200);
                        $response->send();
                    }
                    else {
                        $response = new Response(json_encode($feedback), 500);
                        $response->send();
                    }
                }
                else {
                    $feedback['status'] = 'failed';
                    $feedback['content'] = 'id should be numeric';
                    $response = new Response(json_encode($feedback), 400);
                    $response->send();
                }

            }
            else {
                $feedback['status'] = 'failed';
                $feedback['content'] = 'you didnt send any query';
                $response = new Response(json_encode($feedback), 400);
                $response->send();
            }
        }
        else {
            $feedback['status'] = 'failed';
            $feedback['content'] = 'you should use GET method with find statement';
            $response = new Response(json_encode($feedback), 405);
            $response->send();
        }
    }

    public function all($model, $method) {
        if ($method == 'GET') {
            $modelInstance = new $model(strtolower($model));
            $feedback = $modelInstance->all();
            if ($feedback['status'] = 'success') {
                $response = new Response(json_encode($feedback), 200);
                $response->send();
            }
            else {
                $response = new Response(json_encode($feedback), 500);
                $response->send();
            }
        }
        else {
            $feedback['status'] = 'failed';
            $feedback['content'] = 'you have to use GET in all statement';
            $response = new Response(json_encode($feedback), 405);
            $response->send();
        }
    }

    public function insert($model, $method, $query) {
        if ($method == 'POST') {
            if ($query != '') {
                parse_str($query, $data);
                $modelInstance = new $model(strtolower($model));
                $feedback = $modelInstance->insert($data);
                $response = new Response(json_encode($feedback), 200);
                $response->send();
            }
            else {
                $feedback['status'] = 'failed';
                $feedback['content'] = 'you have entered an empty query';
                $response = new Response(json_encode($feedback), 400);
                $response->send();
            }
        }
        else {
            $feedback['status'] = 'failed';
            $feedback['content'] = 'you have to use POST in insert statement';
            $response = new Response(json_encode($feedback), 405);
            $response->send();
        }
    }

    public function update($model, $method, $query) {
        if (TRUE) {
            if ($query != '') {
                parse_str($query, $data);
                if (is_numeric($data['id'])) {
                    $modelInstance = new $model(strtolower($model));
                    $feedback = $modelInstance->update($data);
                    if ($feedback['status'] == 'success') {
                        $response = new Response(json_encode($feedback), 200);
                        $response->send();
                    }
                    else {
                        $response = new Response(json_encode($feedback), 500);
                        $response->send();
                    }
                }
                else {
                    $feedback['status'] = 'failed';
                    $feedback['content'] = 'id should be numeric';
                    $response = new Response(json_encode($feedback), 400);
                    $response->send();
                }
            }
            else {
                $feedback['status'] = 'failed';
                $feedback['content'] = 'you have entered an empty query';
                $response = new Response(json_encode($feedback), 400);
                $response->send();
            }
        }
        else {
            $feedback['status'] = 'failed';
            $feedback['content'] = 'you have to use PUT or PATCH in update statement';
            $response = new Response(json_encode($feedback), 405);
            $response->send();
        }
    }

    public function delete($model, $method, $query) {
        if ($method == 'DELETE') {
            if ($query != '') {
                parse_str($query, $data);
                if (is_numeric($data['id'])) {
                    $modelInstance = new $model(strtolower($model));
                    $feedback = $modelInstance->delete($data['id']);
                    if ($feedback['status'] == 'success') {
                        $response = new Response(json_encode($feedback), 200);
                        $response->send();
                    } else {
                        $response = new Response(json_encode($feedback), 500);
                        $response->send();
                    }
                }
                else {
                    $feedback['status'] = 'failed';
                    $feedback['content'] = 'id should be numeric';
                    $response = new Response(json_encode($feedback), 400);
                    $response->send();
                }

            }
            else {
                $feedback['status'] = 'failed';
                $feedback['content'] = 'you have entered an empty query';
                $response = new Response(json_encode($feedback), 400);
                $response->send();
            }
        }
        else {
            $feedback['status'] = 'failed';
            $feedback['content'] = 'you have to use DELETE in delete statement';
            $response = new Response(json_encode($feedback), 405);
            $response->send();
        }
    }

}