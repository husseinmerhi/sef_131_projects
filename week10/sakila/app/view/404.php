<?php require_once VIEW . 'layouts/header.php'; ?>

    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center">
                    <h4>ERROR 404</h4>
                </div>
                <div class="card-body">
                    <p>Sorry! The requested page isnt exist.<br>Please make sure from the URL.</p>
                </div>
            </div>
        </div>
    </div>

<?php require_once VIEW . 'layouts/footer.php'; ?>