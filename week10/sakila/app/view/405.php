<?php require_once VIEW . 'layouts/header.php'; ?>

    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center">
                    <h4>ERROR 405</h4>
                </div>
                <div class="card-body">
                    <p>Sorry! The requested action isnt exist.<br>Please check the documentation.</p>
                </div>
            </div>
        </div>
    </div>

<?php require_once VIEW . 'layouts/footer.php'; ?>