<?php require_once VIEW . 'layouts/header.php'; ?>

    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card mb-4">
                <div class="card-header text-primary">
                    Introduction
                </div>
                <div class="card-body">
                    Sakila API allows you to access all records from our database to search for films, actors and dvds and see your personal information and update them. You can also integrate our data into your website.
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header text-primary">
                    Overview
                </div>
                <div class="card-body">
                    You should know that our API is free but you have to give us some credits by inform your clients that the data is supported from Microsoft Inc.
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header text-primary">
                    Response Type & Codes
                </div>
                <div class="card-body">
                    All data returned in JSON object form.<br>JSON object contain 2 fields status and content.<br>Status describe the situation of your request, they are 3 status: Success, Warning, Failed.<br>Use the content to retrieve the requested data <strong>OR</strong> to get more information about your error.<br>All our responses should have one of the following response codes: 200 Success, 400 Bad Request, 405 Method Not Allowed, 500 Internel Server Error.
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header text-primary">
                    Rate Limits
                </div>
                <div class="card-body">
                    For each public IP address, a 100 limit allowed per day, and for security reason the IP will be immediatly blocked after attempting to request 20 requests per second.
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header">
                    Actor
                </div>
                <div class="card-body">
                    <ul>
                        <li>[url]/actor/all</li>
                            <p>to retrieve all actors order by id asc</p>
                        <li>[url]/actor/find?id=[x]</li>
                            <p>
                                to retrieve one actor with id = x.<br>
                                id should be numeric.<br>
                                In case you entered an unexisted id you get response code 200 but with warning status
                            </p>
                        <li>[url]/actor/delete?id=[x]</li>
                            <p>
                                to delete one actor with id = x.<br>
                                id should be numeric.
                            </p>
                        <li>[url]/actor/insert?first_name=[x]&last_name=[x]</li>
                            <p>all fields are required.</p>
                        <li>[url]/actor/update?id=[x]&first_name=[x]&last_name=[x]</li>
                            <p>
                                id field are required.<br>
                                all others are optional but you have to specify at least one.
                            </p>
                    </ul>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header">
                    Customer
                </div>
                <div class="card-body">
                    <ul>
                        <li>[url]/customer/all</li>
                        <p>to retrieve all customers order by id asc</p>
                        <li>[url]/customer/find?id=[x]</li>
                        <p>
                            to retrieve one customer with id = x.<br>
                            id should be numeric.<br>
                            In case you entered an unexisted id you get response code 200 but with warning status
                        </p>
                        <li>[url]/customer/delete?id=[x]</li>
                        <p>
                            to delete one customer with id = x.<br>
                            id should be numeric.
                        </p>
                        <li>[url]/film/insert?store_id=[x]&first_name=[x]&last_name=[x]&email=[x]&address_id=[x]&active=[x]</li>
                        <p>
                            all fields are required.<br>
                            active is 1 or 0.
                        </p>
                        <li>[url]/film/update?id=[x]&store_id=[x]&first_name=[x]&last_name=[x]&email=[x]&address_id=[x]&active=[x]</li>
                        <p>
                            id field are required.<br>
                            all others are optional but you have to specify at least one.
                        </p>
                    </ul>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header">
                    Film
                </div>
                <div class="card-body">
                    <ul>
                        <li>[url]/film/all</li>
                            <p>to retrieve all films order by id asc</p>
                        <li>[url]/film/find?id=[x]</li>
                            <p>
                                to retrieve one film with id = x.<br>
                                id should be numeric.<br>
                                In case you entered an unexisted id you get response code 200 but with warning status
                            </p>
                        <li><span class="text-dark">[url]/film/delete?id=[x]</span> <small class="text-danger">under maintenance</small></li>
                            <p>
                                to delete one film with id = x.<br>
                                id should be numeric.
                            </p>
                        <li>[url]/film/insert?title=[x]&description=[x]&release_year=[x]&language_id=[x]&original_language_id=[x]&rental_duration=[x]&length=[x]&replacement_cost=[x]&rating=[x]</li>
                            <p>
                                all fields are required.<br>
                                rental_duration are in days.<br>
                                length are in minutes.<br>
                                replacement_cost are in dollars.<br>
                                rating are in G, PG, PG-13, R, NC-17
                            </p>
                        <li>[url]/film/update?id=[x]&title=[x]&description=[x]&release_year=[x]&language_id=[x]&original_language_id=[x]&rental_duration=[x]&length=[x]&replacement_cost=[x]&rating=[x]</li>
                            <p>
                                id field are required.<br>
                                all others are optional but you have to specify at least one.
                            </p>
                    </ul>
                </div>
            </div>

        </div>
    </div>

<?php require_once VIEW . 'layouts/footer.php'; ?>