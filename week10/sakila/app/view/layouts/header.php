<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo PREFIX;?>css/app.css"/>
    <link rel="stylesheet" href="<?php echo PREFIX;?>css/style.css"/>

    <title><?php echo $this->getTitle(); ?></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-4">
    <a class="navbar-brand" href="<?php echo PREFIX;?>home">Sakila API Documentation</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php echo ($this->getAction() == 'about')? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo PREFIX;?>home/schema">Schema</a>
            </li>
            <li class="nav-item <?php echo ($this->getAction() == 'contact')? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo PREFIX;?>home/contact">Contact Us</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search ..." aria-label="Search">
            </form>
        </ul>
    </div>
</nav>
