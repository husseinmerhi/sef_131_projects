<?php

class Model {

    protected $link;
    protected $table;
    protected $obj = [];

    public function __construct($table) {
        $this->link = new mysqli(HOST, USER, PASSWORD, DATABASE);
        $this->setTable($table);
    }

    public function find($id) {
        $sql = "select * from {$this->table} where {$this->table}_id = {$id}";
        $result = $this->link->query($sql);
        if ($result) {
            if ($result->num_rows > 0) {
                $this->obj['status'] = 'success';
                $this->obj['content'] = $result->fetch_all(MYSQLI_ASSOC);
            }
            else {
                $this->obj['status'] = 'warning';
                $this->obj['content'] = 'this id doesnt exist';
            }
        }
        else {
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
        }
        return $this->obj;
    }

    public function all() {
        $sql = "select * from {$this->table}";
        $result = $this->link->query($sql);
        if ($result) {
            $this->obj['status'] = 'success';
            if ($result->num_rows > 0) {
                $this->obj['content'] = $result->fetch_all(MYSQLI_ASSOC);
            }
            else {
                $this->obj['content'] = 'table is empty';
            }
        }
        else {
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
        }
        return $this->obj;
    }

    public function insert($data) {
        $columns = '';
        $values = '';
        $sql = "insert into {$this->table}(";
        foreach ($data as $key => $value) {
            if (next($data)) {
                $columns .= "{$key},";
                $values .= "'{$value}',";
            }
            else {
                $columns .= "{$key}) values(";
                $values .= "'{$value}')";
            }
        }
        $sql .= $columns . $values;
        $result = $this->link->query($sql);
        if ($result) {
            $inserted = $this->find($this->link->insert_id);
            if ($inserted['status'] == 'success') {
                $this->obj['status'] = 'success';
                $this->obj['content'] = $inserted['content'];
            }else {
                $this->obj['status'] = 'failed';
                $this->obj['content'] = 'something went wrong with the database, contact the support team for more information';
            }
        }
        else {
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
        }
        return $this->obj;
    }

    public function update($data) {
        $keys = array_keys($data);
        $values = array_values($data);
        $sql = "update {$this->table} set ";
        $id = $data['id'];
        unset($data['id']);
        foreach( $data as $key => $value ) {
            if (next($data)) {
                $sql .= "{$key}='{$value}',";
            }
            else {
                $sql .= "{$key}='{$value}' ";
            }
        }
        $sql .= "where {$this->table}_id={$id}";
        $result = $this->link->query($sql);
        if ($result) {
            $updated = $this->find($id);
            if ($updated['status'] == 'success') {
                $this->obj['status'] = 'success';
                $this->obj['content'] = $updated['content'];
            }else {
                $this->obj['status'] = 'failed';
                $this->obj['content'] = 'something went wrong with the database, contact the support team for more information';
            }
        }
        else {
            $this->obj['status'] = 'Ops';
            $this->obj['content'] = mysqli_error($this->link);
        }
        return $this->obj;
    }

    public function delete($id) {
        $sql = "delete from {$this->table} where {$this->table}_id={$id}";
        $result = $this->link->query($sql);
        if ($result) {
            $deleted = $this->find($id);
            if ($deleted['status'] == 'warning') {
                $this->obj['status'] = 'success';
                $this->obj['content'] = "{$this->table}_id is deleted";
            }else {
                $this->obj['status'] = 'failed';
                $this->obj['content'] = 'something went wrong with the database, contact the support team for more information';
            }
        }
        else {
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
        }
        return $this->obj;

    }

    public function setTable($table) {
        $this->table = $table;
    }

}