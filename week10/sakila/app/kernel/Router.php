<?php

class Router {

    protected $controller = 'HomeController';
    protected $action = 'index';
    protected $parameter = [];

    public function __construct() {
        $this->parseURL();
        $this->route();
    }

    protected function parseURL() {
        $request = trim($_SERVER['PATH_INFO'], '/');
        if (!empty($request)) {
           $url = explode('/', $request);
           $this->controller = ucfirst($url[0]) . 'Controller';
           $this->action = isset($url[1]) ? $url[1] : 'index';
           $this->parameter[] = ucfirst($url[0]);
           $this->parameter[] = $_SERVER['REQUEST_METHOD'];
           $this->parameter[] = $_SERVER['QUERY_STRING'];
        }
    }

    protected function route() {
        if (file_exists(CONTROLLER . $this->controller . '.php')) {
            $this->controller = new $this->controller();
            if (method_exists($this->controller, $this->action)) {
                call_user_func_array([$this->controller, $this->action], $this->parameter);
            }
            else {
                call_user_func([new ErrorController, 'notAllowed']);
            }
        }
        else {
            call_user_func([new ErrorController, 'notFound']);
        }
    }
}