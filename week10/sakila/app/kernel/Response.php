<?php

class Response {

    protected $content;
    protected $statusCode;

    public function __construct($content = '', $statusCode = 200) {
        $this->content = $content;
        $this->statusCode = $statusCode;
    }

    public function send() {
        //$this->content = gzencode($this->content, 9);
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        //header('Accept-Encoding: gzip');
        //header('Vary: Accept-Encoding');
        header( 'Content-Length:' . strlen( $this->content ));
        http_response_code($this->statusCode);
        echo($this->content);
    }
}