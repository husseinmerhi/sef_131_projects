<?php

class View {

    protected $viewFile;
    protected $viewData;
    protected $viewTitle = 'Sakila API';

    public function __construct($viewFile, $viewData) {
        $this->viewFile = $viewFile;
        $this->viewData = $viewData;
    }

    public function render() {
        if (file_exists(VIEW . $this->viewFile . '.php')) {
            include(VIEW . $this->viewFile . '.php');
        }
    }

    public function getAction() {
        return $this->viewFile;
    }

    public function setTitle($title) {
        $this->viewTitle = $title;
    }

    public function getTitle() {
        return $this->viewTitle;
    }

    public function getData() {
        return $this->viewData;
    }
}