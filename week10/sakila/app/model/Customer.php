<?php

class Customer extends Model {

    public function delete($id) {

        $sql = "delete from payment where customer_id={$id}";
        $result = $this->link->query($sql);
        if ($result) {
            $sql = "delete from rental where customer_id={$id}";
            $result2 = $this->link->query($sql);
            if ($result2) {
                $sql = "delete from customer where customer_id={$id}";
                $result3 = $this->link->query($sql);
                if ($result3) {
                    $this->obj['status'] = 'success';
                    $this->obj['content'] = "{$this->table}_id is deleted";
                }else {
                    $this->obj['status'] = 'failed';
                    $this->obj['content'] = 'something went wrong with the database, contact the support team for more information';
                }
            }
            else {
                $this->obj['status'] = 'failed';
                $this->obj['content'] = mysqli_error($this->link);
            }
        }
        else {
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
        }
        return $this->obj;
    }

}