<?php

class Film extends Model {

    public function delete($id) {

        //delete dependencies

        //delete film_actor
        $sql = "delete from film_actor where film_id={$id}";
        $result = $this->link->query($sql);
        if (!$result){
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
            return $this->obj;
        }
        //delete film_category
        $sql = "delete from film_category where film_id={$id}";
        $result = $this->link->query($sql);
        if (!$result){
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
            return $this->obj;
        }
        //delete film_text
        $sql = "delete from film_text where film_id={$id}";
        $result = $this->link->query($sql);
        if (!$result){
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
            return $this->obj;
        }
        //delete inventory
        $sql = "delete from inventory where film_id={$id}";
        $result = $this->link->query($sql);
        if (!$result){
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
            return $this->obj;
        }

        //delete all dependencies from inventory

        //delete target
        $sql = "delete from film where film_id={$id}";
        $result = $this->link->query($sql);
        if ($result){
            $this->obj['status'] = 'success';
            $this->obj['content'] = "{$this->table}_id is deleted";
        }
        else {
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
        }
        return $this->obj;
    }

}