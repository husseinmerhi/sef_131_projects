<?php

class Actor extends Model {

    public function delete($id) {

        $sql = "delete from film_actor where actor_id={$id}";
        $result = $this->link->query($sql);
        if ($result) {
            $sql = "delete from actor where actor_id={$id}";
            $result2 = $this->link->query($sql);
            if ($result2) {
                $this->obj['status'] = 'success';
                $this->obj['content'] = "{$this->table}_id is deleted";
            }else {
                $this->obj['status'] = 'failed';
                $this->obj['content'] = 'something went wrong with the database, contact the support team for more information';
            }
        }
        else {
            $this->obj['status'] = 'failed';
            $this->obj['content'] = mysqli_error($this->link);
        }
        return $this->obj;
    }

}