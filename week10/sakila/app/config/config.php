<?php

    define('ROOT', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR);
    define('SERVER_ROOT', $_SERVER['DOCUMENT_ROOT']);
    define('PREFIX', substr(ROOT, strlen(SERVER_ROOT)) . 'public' . DIRECTORY_SEPARATOR);
    define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);
    define('KERNEL', ROOT . 'app' . DIRECTORY_SEPARATOR . 'kernel' . DIRECTORY_SEPARATOR);
    define('CONTROLLER', ROOT . 'app' . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR);
    define('MODEL', ROOT . 'app' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR);
    define('VIEW', ROOT . 'app' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR);
    define('PUB', ROOT . 'public' . DIRECTORY_SEPARATOR);

    define('HOST', 'localhost');
    define('DATABASE', 'sakila');
    define('USER', 'phpuser');
    define('PASSWORD', 'admin@123');