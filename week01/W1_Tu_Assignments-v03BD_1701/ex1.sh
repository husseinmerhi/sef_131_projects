#!/bin/bash
clear
ls -l /var/log/*.log | awk '{print $9,$5/1024}' | while read output;
do
	name_file=$(echo $output | awk '{print $1}' | cut -d'/'  -f4)
	size_file=$(echo $output | awk '{print $2}')
	size_file=${size_file%.*}
	echo "$name_file,$size_file" >> log_dump.csv
done
head log_dump.csv
