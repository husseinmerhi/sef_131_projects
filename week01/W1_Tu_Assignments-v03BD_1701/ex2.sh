#!/bin/bash
clear
fine=1
virtual_memory_utilization=0
threshold=80
#search for used memory divided by total memory
virtual_memory_utilization=`free | grep Mem | awk '{print ($3/$2)*100}'`
#convert float to int
virtual_memory_utilization=${virtual_memory_utilization%.*}
#threshold
if [ "$virtual_memory_utilization" -ge "$threshold" ];
then
	fine=0
	echo "ALARM: Virtual Memory is at $virtual_memory_utilization%"
fi
#search for used disk percentage
df | grep /dev.sda* | awk '{print $1,$5}' | while read output;
do
	name_partition=$(echo $output | awk '{print $1}')
	percentage=$(echo $output | awk '{print $2}' | cut -d'%' -f1)
	if [ "$percentage" -ge "$threshold" ];
	then
		fine=0
		echo "ALARM: $name_partition is at $percentage%"
	fi
done
#test if everything is fine
if [ "$fine" == 1 ];
then
	echo "Everything is Fine!"
fi