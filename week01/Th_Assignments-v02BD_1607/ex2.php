<?php

access_log();

function access_log(){
	try{
		$file = fopen("/var/log/apache2/access.log","r");
		$line = fgets($file);
		while(!feof($file)){
			//extract ip
			$ip = explode(" ",$line);
			echo $ip[0]." -- ";
			//extract datetime and parse it to another format
			$date = explode("[",$line);
			$date = explode(" ",$date[1]);
			$date = DateTime::createFromFormat('d/M/Y:H:i:s',$date[0]);
			echo $date->format('l, F d Y : H-i-s')." -- ";
			//extract request
			$request = explode("\"",$line);
			echo $request[1]." -- ";
			//extract response
			echo $ip[8];
			echo "\n";
			$line = fgets($file);
		}
	}catch(Exception $ex){
		echo $ex->getMessage();
	}
}

?>