<?php 

$options = getopt("i:");
echo "files in ".$options["i"].":\n";

list_files($options["i"]);

function list_files($path){
	try{
		if(file_exists($path)){
			$root = new RecursiveDirectoryIterator($path);
			foreach($root as $value){
				if(is_file($value)){
					echo basename($value)."\n";
				}else{
					//we dont need to look in . and .. directory to avoiding infinity loop
					if($value != $path."/." && $value != $path."/.."){
						list_files($value);
					}
				}
			}
		}else{
			echo "file isnt exist\n";
		}
	}catch(Exception $ex){
		echo $ex->getMessage();
	}
}

?>