#!/usr/bin/php
<?php

$hats=array('white','white','black','black','white',
			'black','white','white','black','white'
		   );

result($hats,solve($hats));

function solve($hats){
	$guess = first_guess($hats);
	$answer[] = $guess;
	for($i=1;$i<sizeof($hats);$i++){
		$answer[] = second_guess($hats,$guess,$i);	
	}
	return $answer;
}

function first_guess($hats){
	return parity($hats,0);
}

function parity($hats,$position){
	$occurrence = 0;
	for($i=$position+1;$i<sizeof($hats);$i++){
		if($hats[$i] == 'white'){
			$occurrence++;
		}
	}
	if($occurrence%2==0){
		return 'white';
	}else{
		return 'black';
	}
}
function second_guess($hats,&$guess,$position){
	if(parity($hats,$position)==$guess){
		return 'black';
	}else{
		if($guess == 'white'){
			$guess = 'black';
		}else{
			$guess = 'white';
		}
		return 'white';
	}
}

function result($hats,$answer){
	$error = 0;
	for($i=0;$i<sizeof($hats);$i++){
		if($hats[$i]!=$answer[$i]){
			$error++;
		}
	}
	print_r($hats);
	print_r($answer);
	if($error<2){
		echo "the humanity will survive!\n";
	}else{
		echo "Goodbye!\n";
	}
}

?>