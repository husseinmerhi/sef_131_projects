SELECT a2.first_name, a2.last_name
FROM actor as a1, actor as a2
WHERE a1.actor_id = 8
AND a1.actor_id != a2.actor_id
AND a1.first_name = a2.first_name

UNION ALL

SELECT customer.first_name, customer.last_name
FROM actor, customer
WHERE actor.actor_id = 8
AND actor.first_name = customer.first_name;