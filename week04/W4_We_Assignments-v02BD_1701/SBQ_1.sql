SELECT first_name, last_name, COUNT(film_actor.film_id)
FROM actor,film_actor
WHERE actor.actor_id = film_actor.actor_id
GROUP BY actor.actor_id;