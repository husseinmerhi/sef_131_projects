SELECT category.name, COUNT(film.film_id)
FROM category, film, film_category
WHERE category.category_id = film_category.category_id
AND film.film_id = film_category.film_id
GROUP BY category.category_id
HAVING COUNT(film.film_id) BETWEEN 55 AND 65

UNION

SELECT category.name, COUNT(film.film_id)
FROM category,film,film_category
WHERE category.category_id = film_category.category_id
AND film.film_id = film_category.film_id
AND NOT EXISTS(
	SELECT category.name, COUNT(film.film_id)
	FROM category,film,film_category
	WHERE category.category_id = film_category.category_id
	AND film.film_id = film_category.film_id
	GROUP BY category.category_id
	HAVING COUNT(film.film_id) BETWEEN 55 AND 65
)
GROUP BY category.category_id
ORDER BY potato DESC LIMIT 1
;