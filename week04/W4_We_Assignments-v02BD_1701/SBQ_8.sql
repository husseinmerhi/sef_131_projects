SELECT store.store_id, SUM(payment.amount), 
					   AVG(payment.amount),
                       MONTH(rental.rental_date),
                       YEAR(rental.rental_date)
FROM rental,payment,store,staff
WHERE payment.rental_id = rental.rental_id
AND payment.staff_id = staff.staff_id
AND staff.store_id = store.store_id
GROUP BY store_id, MONTH(rental.rental_date), YEAR(rental.rental_date);