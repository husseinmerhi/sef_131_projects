SELECT customer.first_name, customer.last_name, COUNT(rental.rental_id)
FROM customer,rental
WHERE rental.customer_id = customer.customer_id
AND YEAR(rental.rental_date) = '2005'
GROUP BY rental.customer_id
ORDER BY COUNT(rental.rental_id)
DESC LIMIT 3;