SELECT 
    first_name, last_name, release_year
FROM
    actor,
    film_actor,
    film
WHERE
    actor.actor_id = film_actor.actor_id
        AND film_actor.film_id = film.film_id
        AND (film.description LIKE '%crocodile%'
        OR film.description LIKE '%shark%')
GROUP BY first_name , last_name , release_year
ORDER BY last_name;