SELECT language.name, COUNT(language.name)
FROM film,language
WHERE film.release_year = 2006
AND film.language_id = language.language_id
GROUP BY language.name
ORDER BY COUNT(language.name)
DESC LIMIT 3;