SELECT p1.proc_id,p1.start_time,p1.end_time,p2.proc_id,p2.start_time,p2.end_time
FROM anest_procedure AS p1
JOIN anest_procedure AS p2
ON p1.anest_name = p2.anest_name
AND p1.proc_id != p2.proc_id
WHERE p2.start_time BETWEEN p1.start_time AND p1.end_time
	OR p2.end_time BETWEEN p1.start_time AND p1.end_time
    OR (p2.start_time < p1.start_time AND p2.end_time > p1.end_time)
ORDER BY p1.proc_id,p2.proc_id;

SELECT id1, MAX(total) AS max_inst_count
FROM (
	SELECT P1.proc_id AS id1, P2.proc_id, COUNT(*) AS total
    FROM anest_procedure AS P1, anest_procedure AS P2, anest_procedure AS P3
    WHERE P2.anest_name = P1.anest_name
		AND P3.anest_name = P1.anest_name
        AND P1.start_time <= P2.start_time
        AND P2.start_time < P1.end_time
        AND P3.start_time <= P2.start_time
        AND P2.start_time < P3.end_time
    GROUP BY P1.proc_id , P2.proc_id) AS result
GROUP BY id1;