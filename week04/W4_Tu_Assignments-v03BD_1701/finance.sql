CREATE DATABASE IF NOT EXISTS FinanceDB;
USE FinanceDB;

CREATE TABLE IF NOT EXISTS FiscalYearTable (
	fiscal_year INTEGER NOT NULL PRIMARY KEY,
	start_date DATE NOT NULL,
	end_date DATE NOT NULL
);

DROP TRIGGER IF EXISTS validate;
DELIMITER $$
CREATE TRIGGER validate BEFORE INSERT ON FiscalYearTable
	FOR EACH ROW
	BEGIN
		IF (    YEAR (NEW.start_date) != NEW.fiscal_year -1 
			 || MONTH (NEW.start_date) != 10
             || DAY (NEW.start_date) != 1
		) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'error1';
		ELSEIf (    YEAR (NEW.end_date) != NEW.fiscal_year 
				 || MONTH (NEW.end_date) != 9
				 || DAY (NEW.end_date) != 30
		) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'error2';
		END IF;
	END
$$
DELIMITER ;