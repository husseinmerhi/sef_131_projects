CREATE DATABASE IF NOT EXISTS Court;
USE Court;

CREATE TABLE IF NOT EXISTS claim (
    claim_id INT(10) NOT NULL AUTO_INCREMENT,
    patient_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (claim_id)
);

CREATE TABLE IF NOT EXISTS defendant (
    claim_id INT(10) NOT NULL,
    defendant_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (claim_id , defendant_name)
);

CREATE TABLE IF NOT EXISTS legal_event (
    claim_id INT(10) NOT NULL,
    defendant_name VARCHAR(255) NOT NULL,
    claim_status VARCHAR(255) NOT NULL,
    change_date DATE NOT NULL,
    PRIMARY KEY (claim_id , defendant_name , claim_status)
);

CREATE TABLE IF NOT EXISTS claim_status_code (
    claim_status VARCHAR(255) NOT NULL,
    claim_status_description VARCHAR(255) NOT NULL,
    claim_seq INT(10) NOT NULL,
    PRIMARY KEY (claim_seq)
);

INSERT INTO claim (patient_name) values
	('Bassem Dghaidi'),
    ('Omar Breidi'),
    ('Marwan Sawwan');
    
INSERT INTO defendant values
	(1,'Jean Skaff'),
    (1,'Elie Meouchi'),
    (1,'Radwan Sameh'),
    (2,'Joseph Eid'),
    (2,'Paul Syoufi'),
    (2,'Radwan Sameh'),
    (3,'Issam Awwad');
    
INSERT INTO legal_event values
	(1,'Jean Skaff','AP','2016-01-01'),
    (1,'Jean Skaff','OR','2016-02-02'),
    (1,'Jean Skaff','SF','2016-03-01'),
    (1,'Jean Skaff','CL','2016-04-01'),
    (1,'Radwan Sameh','AP','2016-01-01'),
    (1,'Radwan Sameh','OR','2016-02-02'),
    (1,'Radwan Sameh','SF','2016-03-01'),
    (1,'Elie Meouchi','AP','2016-01-01'),
    (1,'Elie Meouchi','OR','2016-02-02'),
    (2,'Radwan Sameh','AP','2016-01-01'),
    (2,'Radwan Sameh','OR','2016-02-01'),
    (2,'Paul Syoufi','AP','2016-01-01'),
    (3,'Issam Awwad','AP','2016-01-01');
    
INSERT INTO claim_status_code values
	('AP','Awaiting review panel',1),
    ('OR','Panel opinion rendered',2),
    ('SF','Suit filed',3),
    ('CL','Closed',4);