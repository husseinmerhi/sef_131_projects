SELECT c.claim_id,c.patient_name,csc.claim_status
FROM (
		SELECT claim_id,MIN(max_status) AS min_status
		FROM (
			SELECT le.claim_id, le.defendant_name, max(csc.claim_seq) as max_status
			FROM legal_event AS le
			JOIN claim_status_code AS csc ON le.claim_status = csc.claim_status
			GROUP BY le.claim_id,le.defendant_name
		) AS t1
		GROUP BY claim_id
	) AS t2
JOIN claim AS c ON t2.claim_id = c.claim_id
JOIN claim_status_code AS csc ON t2.min_status = csc.claim_seq
ORDER BY c.claim_id;